package com.streams.eda.samples.shipping.processor;

import com.streams.samples.*;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Slf4j
@EnableBinding(OrderShippingProcessor.StreamProcessor.class)
public class OrderShippingProcessor {
    @Autowired
    private Serde<OrderKey> keySerde;
    @Autowired
    @Qualifier("aggregatedOrderItemStockReservedSerde")
    private Serde<AggregatedOrderItemStockReserved> aggregatedOrderItemStockReservedSerde;
    @Autowired
    @Qualifier("orderItemStockReservedSerde")
    private Serde<OrderItemStockReserved> orderItemStockReservedSerde;
    @Autowired
    @Qualifier("orderStockReserveCompletedSerde")
    private Serde<OrderStockReserveCompleted> orderStockReserveCompletedSerde;

    @StreamListener
    @SendTo({"orderShipped", "orderWorkflowStepCompleted"})
    public KStream<?, ?>[] process(@Input("orderItemStockReserved") KStream<OrderKey, OrderItemStockReserved> orderItemStockReservedStream,
                                   @Input("orderStockReserveCompleted") KStream<OrderKey, OrderStockReserveCompleted> orderStockReserveCompletedStream) {

        //Randomly filter orders to simulate missing some orders to test expired orders
        orderStockReserveCompletedStream = orderStockReserveCompletedStream.filter(this::filterOrder);
        orderStockReserveCompletedStream.peek((k, v) -> log.info("Started to ship order with ID {}", k));

        //Complete list of stock reserve for order items
        KStream<OrderKey, AggregatedOrderItemStockReserved> accumulatedOrderItemStockReserve = accumulateOrderItemStockReservedEvents(orderItemStockReservedStream)
                .toStream()
                .filter((k, v) -> v.getAggregatedReservedItems().size() == v.getTotalOrderItems())
                .peek((k, v) -> log.info("The order item stock reservation of order with ID {} has been fully completed", k));

        var orderShippedStream = toOrderShippedStream(orderStockReserveCompletedStream, accumulatedOrderItemStockReserve);
        var workflowStepCompletedStream = orderShippedStream.mapValues(this::toStepCompleted);

        return new KStream[]{orderShippedStream, workflowStepCompletedStream};
    }

    private boolean filterOrder(OrderKey orderKey, OrderStockReserveCompleted orderStockReserveCompleted) {
        return orderStockReserveCompleted.getTotalOrderItems() != 3;
    }

    private KTable<OrderKey, AggregatedOrderItemStockReserved> accumulateOrderItemStockReservedEvents(@Input("orderItemStockReserved") KStream<OrderKey, OrderItemStockReserved> orderItemStockReservedStream) {
        Materialized<OrderKey, AggregatedOrderItemStockReserved, KeyValueStore<Bytes, byte[]>> materialized =
                Materialized.<OrderKey, AggregatedOrderItemStockReserved, KeyValueStore<Bytes, byte[]>>as("order-items-stock-reserve-acc")
                        .withKeySerde(keySerde).withValueSerde(aggregatedOrderItemStockReservedSerde);

        Aggregator<OrderKey, OrderItemStockReserved, AggregatedOrderItemStockReserved> aggregator = (k, v, acc) -> this.aggregateOrderItemsStockReserve(acc, v);
        Serialized<OrderKey, OrderItemStockReserved> serialized = Serialized.with(keySerde, orderItemStockReservedSerde);

        return orderItemStockReservedStream.groupByKey(serialized)
                .aggregate(AggregatedOrderItemStockReserved::new, aggregator, materialized);
    }

    private OrderShipped toOrderShipped(Tuple2<OrderStockReserveCompleted, AggregatedOrderItemStockReserved> value) {
        var userId = UUID.randomUUID().toString();
        var trackingNumber = UUID.randomUUID().toString();
        List<OrderItem> orderItems = value._2.getAggregatedReservedItems().stream()
                .map(OrderItemStockReserved::getItem)
                .collect(Collectors.toList());

        return new OrderShipped(userId, trackingNumber, Instant.now().toEpochMilli(), orderItems);
    }

    private AggregatedOrderItemStockReserved aggregateOrderItemsStockReserve(AggregatedOrderItemStockReserved acc, OrderItemStockReserved v) {
        //Avro does not yet initialise the list with empty value although of default [].
        var updatedAggregatedReservedItems = Optional.ofNullable(acc.getAggregatedReservedItems())
                .orElse(new ArrayList<>());
        updatedAggregatedReservedItems.add(v);
        acc.setAggregatedReservedItems(updatedAggregatedReservedItems);

        acc.setTotalOrderItems(v.getTotalOrderItems());
        return acc;
    }

    private KStream<OrderKey, OrderShipped> toOrderShippedStream(KStream<OrderKey, OrderStockReserveCompleted> orderStockReserveCompletedStream,
                                                                 KStream<OrderKey, AggregatedOrderItemStockReserved> aggregatedOrderItemStockReservedStream) {

        //Join the aggregated order item stock reserves with order completed stock reservation to simulate the fact that
        //Shipping order items might happen in separate steps instead of shipping the whole order in one step.
        //When stock reserve for all items are received, we then can say that the order is shipped.
        return orderStockReserveCompletedStream.join(aggregatedOrderItemStockReservedStream, Tuple::of, joinWindow(),  joined())
                .mapValues(this::toOrderShipped);
    }

    private JoinWindows joinWindow() {
        return JoinWindows.of(Duration.of(1, ChronoUnit.HOURS).toMillis());
    }

    private Joined<OrderKey, OrderStockReserveCompleted, AggregatedOrderItemStockReserved> joined() {
        return Joined.with(
                keySerde,
                orderStockReserveCompletedSerde,
                aggregatedOrderItemStockReservedSerde);
    }

    private OrderWorkflowStepCompleted toStepCompleted(OrderKey orderKey, OrderShipped orderShipped) {
        return new OrderWorkflowStepCompleted("order-shipped", "order-stock-reserved", Instant.now().toEpochMilli());
    }

    public interface StreamProcessor {
        @Input("orderItemStockReserved")
        KStream<?, ?> orderItemStockReserved();

        @Input("orderStockReserveCompleted")
        KStream<?, ?> orderStockReserveCompleted();

        @Output("orderWorkflowStepCompleted")
        KStream<?, ?> orderWorkflowStepCompleted();

        @Output("orderShipped")
        KStream<?, ?> orderShipped();
    }
}
