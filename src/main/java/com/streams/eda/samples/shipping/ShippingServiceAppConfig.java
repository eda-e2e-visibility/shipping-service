package com.streams.eda.samples.shipping;

import com.streams.samples.AggregatedOrderItemStockReserved;
import com.streams.samples.OrderItemStockReserved;
import com.streams.samples.OrderKey;
import com.streams.samples.OrderStockReserveCompleted;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Serde;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.Map;

@Configuration
public class ShippingServiceAppConfig {
    @Value("${spring.cloud.stream.kafka.streams.binder.configuration.schema.registry.url}")
    private String endPoint;

    @Bean
    public SchemaRegistryClient schemaRegistryClient() {
        ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
        client.setEndpoint(endPoint);
        return client;
    }

    @Bean
    Serde<OrderKey> keySerde() {
        return serdeFor(true);
    }

    @Bean("aggregatedOrderItemStockReservedSerde")
    Serde<AggregatedOrderItemStockReserved> aggregatedOrderItemStockReservedSerde() {
        return serdeFor(false);
    }

    @Bean("orderItemStockReservedSerde")
    Serde<OrderItemStockReserved> orderItemStockReservedSerde() {
        return serdeFor(false);
    }

    @Bean("orderStockReserveCompletedSerde")
    Serde<OrderStockReserveCompleted> orderStockReserveCompletedSerde() {
        return serdeFor(false);
    }

    private <T extends SpecificRecord> Serde<T> serdeFor(boolean isKey) {
        Map<String, String> serdeConfig = Collections.singletonMap("schema.registry.url", endPoint);

        Serde<T> serde = new SpecificAvroSerde<>();
        serde.configure(serdeConfig, isKey);

        return serde;
    }
}
