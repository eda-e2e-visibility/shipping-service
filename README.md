# Order shipping service

Manage shipping of orders to customers. The following diagram shows the events to and from the service.

<p align="center">
  <img  width="700" src="docs/order-shipping-service.png?raw=true" title="Order shipping service">
</p>

## How it works

This service is consuming all `order-item-stock-reserved` events and aggregate them all before joining with `order-stock-reserve-completed`.
The idea is to wait for the stock of all order items to be reserved, after that join with `order-stock-reserve-completed` to mark order as shipped.
The service is aggregating all received `order-item-stock-reserved` and filter out until it receives all stock reservations for that order.
The following code shows how it is working

```java
//Complete list of stock reserve for order items
KStream<OrderKey, AggregatedOrderItemStockReserved> accumulatedOrderItemStockReserve = accumulateOrderItemStockReservedEvents(orderItemStockReservedStream)
    .toStream()
    .filter((k, v) -> v.getAggregatedReservedItems().size() == v.getTotalOrderItems())
    .peek((k, v) -> log.info("The order item stock reservation of order with ID {} has been fully completed", k));
```

I chose to implement it that way because of the following reasons

* Decouple stock service from shipping service where at any point if stock service decided to publish `order-item-stock-reserved` events with long interval between them.
(like the stock is not ready for some order items), and it takes hours to complete and publish `order-stock-reserve-completed` event. So there should not be a restriction of join window length.
*  Shipping service can support shipping individual order items that are ready for shipping(ready here means stock is reserved).

## Topology DAG

The following DAG shows deep view of how shipping service is handling stock reservations.

<p align="center">
  <img  width="700" src="docs/order-shipping-DAG.png?raw=true" title="Order shipping service">
</p>